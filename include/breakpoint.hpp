#pragma once

#include <sys/ptrace.h>
#include <unistd.h>

#include <cstdint>

namespace minidbg {

class breakpoint {
 public:
  breakpoint() = default;
  breakpoint(const pid_t pid, const std::intptr_t addr)
      : m_pid(pid), m_addr(addr), m_enabled(false) {}

  void enable();
  void disable();

  auto is_enabled() const { return m_enabled; }
  auto get_addr() const { return m_addr; }

 private:
  pid_t m_pid;
  std::intptr_t m_addr;
  bool m_enabled;
  std::uint8_t m_saved_data;
};

}  // namespace minidbg
