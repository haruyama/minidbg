#pragma once

#include <fcntl.h>
#include <unistd.h>

#include <csignal>
#include <string>
#include <unordered_map>
#include <utility>
#include <vector>

#include "breakpoint.hpp"
#include "dwarf/dwarf++.hh"
#include "elf/elf++.hh"

namespace minidbg {

enum class symbol_type {
  notype,
  object,
  func,
  section,
  file,
};

std::string to_string(symbol_type st);

struct symbol {
  symbol_type type;
  std::string name;
  std::uintptr_t addr;
};

class debugger {
 public:
  debugger(const std::string& prog_name, const pid_t pid)
      : m_prog_name(prog_name), m_pid(pid) {
    auto fd = open(m_prog_name.c_str(), O_RDONLY);
    m_elf = elf::elf(elf::create_mmap_loader(fd));
    m_dwarf = dwarf::dwarf(dwarf::elf::create_loader(m_elf));
  }
  void run();

 private:
  void handle_command(const std::string& line);
  void continue_execution();
  void dump_registers();
  std::uint64_t read_memory(std::uint64_t address);
  void write_memory(std::uint64_t address, std::uint64_t value);
  std::uint64_t get_pc();
  void set_pc(std::uint64_t pc);
  void step_over_breakpoint();
  void wait_for_signal();
  dwarf::die get_function_from_pc(std::uint64_t pc);
  dwarf::line_table::iterator get_line_entry_from_pc(std::uint64_t pc);
  void initialize_load_address();
  std::uint64_t offset_load_address(std::uint64_t addr);
  void print_source(const std::string& file_name, unsigned int line,
                    unsigned int n_lines_context = 2);
  siginfo_t get_signal_info();
  void handle_sigtrap(siginfo_t info);
  void single_step_instruction();
  void single_step_instruction_with_breakpoint_check();
  void step_in();
  void step_out();
  void step_over();
  void remove_breakpoint(const std::intptr_t addr);
  std::uint64_t get_offset_pc();
  std::uint64_t offset_dwarf_address(std::uint64_t addr);
  void set_breakpoint_at_address(const std::intptr_t addr);
  void set_breakpoint_at_function(const std::string& name);
  void set_breakpoint_at_source_line(const std::string& file,
                                     unsigned int line);
  std::vector<symbol> lookup_symbol(const std::string& name);
  void print_backtrace();
  void read_variables();

  std::string m_prog_name;
  pid_t m_pid;
  std::unordered_map<std::intptr_t, breakpoint> m_breakpoints;
  dwarf::dwarf m_dwarf;
  elf::elf m_elf;
  std::uint64_t m_load_address = 0;
};

}  // namespace minidbg
