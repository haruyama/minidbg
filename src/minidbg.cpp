#include <sys/personality.h>
#include <sys/ptrace.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include <algorithm>
#include <csignal>
#include <cstdint>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include "breakpoint.hpp"
#include "debugger.hpp"
#include "dwarf/dwarf++.hh"
#include "linenoise.h"
#include "registers.hpp"

bool is_prefix(const std::string& s, const std::string& of) {
  if (s.size() > of.size()) return false;
  return std::equal(s.begin(), s.end(), of.begin());
}

bool is_suffix(const std::string& s, const std::string& of) {
  if (s.size() > of.size()) return false;
  auto diff = of.size() - s.size();
  return std::equal(s.begin(), s.end(),
                    of.begin() + static_cast<std::int64_t>(diff));
}

class ptrace_expr_content : public dwarf::expr_context {
 public:
  explicit ptrace_expr_content(pid_t pid) : m_pid(pid) {}

  dwarf::taddr reg(unsigned int regnum) override {
    return minidbg::get_register_value_from_dwarf_register(m_pid, regnum);
  }

  dwarf::taddr pc() {
    struct user_regs_struct regs;
    ptrace(PTRACE_GETREGS, m_pid, nullptr, &regs);
    return regs.rip;
  }

  dwarf::taddr deref_size(dwarf::taddr address, unsigned int) override {
    return static_cast<dwarf::taddr>(
        ptrace(PTRACE_PEEKDATA, m_pid, address, nullptr));
  }

 private:
  pid_t m_pid;
};

namespace minidbg {
std::string to_string(symbol_type st) {
  switch (st) {
    case symbol_type::notype:
      return "notype";
    case symbol_type::object:
      return "object";
    case symbol_type::func:
      return "func";
    case symbol_type::section:
      return "section";
    case symbol_type::file:
      return "file";
  }
}

symbol_type to_symbol_type(elf::stt sym) {
  switch (sym) {
    case elf::stt::notype:
      return symbol_type::notype;
    case elf::stt::object:
      return symbol_type::object;
    case elf::stt::func:
      return symbol_type::func;
    case elf::stt::section:
      return symbol_type::section;
    case elf::stt::file:
      return symbol_type::file;
    default:
      return symbol_type::notype;
  }
}

std::uint64_t get_register_value(pid_t pid, reg r) {
  user_regs_struct regs;
  ptrace(PTRACE_GETREGS, pid, nullptr, &regs);
  auto it = std::find_if(std::begin(g_register_descriptors),
                         std::end(g_register_descriptors),
                         [r](auto&& rd) { return rd.r == r; });
  return *(reinterpret_cast<std::uint64_t*>(&regs) +
           (it - std::begin(g_register_descriptors)));
}

void set_register_value(pid_t pid, reg r, std::uint64_t value) {
  user_regs_struct regs;
  ptrace(PTRACE_GETREGS, pid, nullptr, &regs);
  auto it = std::find_if(std::begin(g_register_descriptors),
                         std::end(g_register_descriptors),
                         [r](auto&& rd) { return rd.r == r; });
  *(reinterpret_cast<std::uint64_t*>(&regs) +
    (it - std::begin(g_register_descriptors))) = value;
  ptrace(PTRACE_SETREGS, pid, nullptr, &regs);
}

std::uint64_t get_register_value_from_dwarf_register(pid_t pid,
                                                     unsigned int regnum) {
  auto it = std::find_if(
      std::begin(g_register_descriptors), std::end(g_register_descriptors),
      [regnum](auto&& rd) { return rd.dwarf_r == static_cast<int>(regnum); });
  if (it == std::end(g_register_descriptors)) {
    throw std::out_of_range{"Unknown dwarf register"};
  }

  return get_register_value(pid, it->r);
}

std::string get_register_name(reg r) {
  auto it = std::find_if(std::begin(g_register_descriptors),
                         std::end(g_register_descriptors),
                         [r](auto&& rd) { return rd.r == r; });
  return it->name;
}

reg get_register_from_name(const std::string& name) {
  auto it = std::find_if(std::begin(g_register_descriptors),
                         std::end(g_register_descriptors),
                         [name](auto&& rd) { return rd.name == name; });
  return it->r;
}

void breakpoint::enable() {
  auto data = ptrace(PTRACE_PEEKDATA, m_pid, m_addr, nullptr);
  m_saved_data = static_cast<std::uint8_t>(data & 0xff);
  std::uint64_t int3 = 0xcc;
  std::uint64_t data_with_int3 =
      ((static_cast<std::uint64_t>(data & ~0xff)) | int3);
  ptrace(PTRACE_POKEDATA, m_pid, m_addr, data_with_int3);
  m_enabled = true;
}

void breakpoint::disable() {
  auto data = ptrace(PTRACE_PEEKDATA, m_pid, m_addr, nullptr);
  auto restored_data =
      ((static_cast<std::uint64_t>(data & ~0xff)) | m_saved_data);
  ptrace(PTRACE_POKEDATA, m_pid, m_addr, restored_data);

  m_enabled = false;
}

std::vector<std::string> split(const std::string& s, char delimiter) {
  std::vector<std::string> out{};
  std::stringstream ss{s};
  std::string item;

  while (std::getline(ss, item, delimiter)) {
    out.push_back(item);
  }
  return out;
}

void debugger::run() {
  wait_for_signal();
  initialize_load_address();

  char* line = nullptr;
  while ((line = linenoise("minidbg> ")) != nullptr) {
    handle_command(line);
    linenoiseHistoryAdd(line);
    linenoiseFree(line);
  }
}

void debugger::handle_command(const std::string& line) {
  auto args = split(line, ' ');
  auto command = args[0];

  if (is_prefix(command, "cont")) {
    continue_execution();
  } else if (is_prefix(command, "break")) {
    if (is_prefix(args[1], "0x")) {
      std::string addr{args[1], 2};
      set_breakpoint_at_address(std::stol(addr, 0, 16));
    } else if (args[1].find(':') != std::string::npos) {
      auto file_and_line = split(args[1], ':');
      set_breakpoint_at_source_line(
          file_and_line[0],
          static_cast<unsigned int>(std::stoul(file_and_line[1])));
    } else {
      set_breakpoint_at_function(args[1]);
    }
  } else if (is_prefix(command, "register")) {
    if (is_prefix(args[1], "dump")) {
      dump_registers();
    } else if (is_prefix(args[1], "read")) {
      std::cout << get_register_value(m_pid,
                                      minidbg::get_register_from_name(args[2]))
                << '\n';
    } else if (is_prefix(args[1], "write")) {
      std::string val{args[3], 2};
      set_register_value(m_pid, minidbg::get_register_from_name(args[2]),
                         std::stoul(val, 0, 16));
    }
  } else if (is_prefix(command, "memory")) {
    std::string addr{args[2], 2};
    if (is_prefix(args[1], "read")) {
      std::cout << std::hex << read_memory(std::stoul(addr, 0, 16)) << '\n';
    } else if (is_prefix(args[1], "write")) {
      std::string val{args[3], 2};
      write_memory(std::stoul(addr, 0, 16), std::stoul(val, 0, 16));
    }
  } else if (is_prefix(command, "stepi")) {
    single_step_instruction_with_breakpoint_check();
    auto line_entry = get_line_entry_from_pc(get_pc());
    print_source(line_entry->file->path, line_entry->line);
  } else if (is_prefix(command, "step")) {
    step_in();
  } else if (is_prefix(command, "next")) {
    step_over();
  } else if (is_prefix(command, "finish")) {
    step_out();
  } else if (is_prefix(command, "symbol")) {
    auto syms = lookup_symbol(args[1]);
    for (const auto& s : syms) {
      std::cout << s.name << ' ' << to_string(s.type) << " 0x" << std::hex
                << s.addr << '\n';
    }
  } else if (is_prefix(command, "backtrace")) {
    print_backtrace();
  } else if (is_prefix(command, "variables")) {
    read_variables();
  } else {
    std::cerr << "Unknown command\n";
  }
}

void debugger::continue_execution() {
  step_over_breakpoint();
  ptrace(PTRACE_CONT, m_pid, nullptr, nullptr);
  wait_for_signal();
}

void debugger::set_breakpoint_at_address(const std::intptr_t addr) {
  std::cout << "Set breakpint at address 0x" << std::hex << addr << "\n";
  breakpoint bp{m_pid, addr};
  bp.enable();
  m_breakpoints[addr] = bp;
}

void debugger::set_breakpoint_at_function(const std::string& name) {
  for (const auto& cu : m_dwarf.compilation_units()) {
    for (const auto& die : cu.root()) {
      if (die.has(dwarf::DW_AT::name) && at_name(die) == name) {
        auto low_pc = at_low_pc(die);
        auto entry = get_line_entry_from_pc(low_pc);
        ++entry;
        set_breakpoint_at_address(
            static_cast<std::intptr_t>(offset_dwarf_address(entry->address)));
      }
    }
  }
}

void debugger::set_breakpoint_at_source_line(const std::string& file,
                                             unsigned int line) {
  for (const auto& cu : m_dwarf.compilation_units()) {
    if (is_suffix(file, at_name(cu.root()))) {
      const auto& lt = cu.get_line_table();

      for (const auto& entry : lt) {
        if (entry.is_stmt && entry.line == line) {
          set_breakpoint_at_address(
              static_cast<std::intptr_t>(offset_dwarf_address(entry.address)));
          return;
        }
      }
    }
  }
}

void debugger::dump_registers() {
  for (const auto& rd : minidbg::g_register_descriptors) {
    std::cout << rd.name << " 0x" << std::setfill('0') << std::setw(16)
              << std::hex << get_register_value(m_pid, rd.r) << '\n';
  }
}

std::uint64_t debugger::read_memory(std::uint64_t address) {
  return static_cast<std::uint64_t>(
      ptrace(PTRACE_PEEKDATA, m_pid, address, nullptr));
}

void debugger::write_memory(std::uint64_t address, std::uint64_t value) {
  ptrace(PTRACE_POKEDATA, m_pid, address, value);
}

std::uint64_t debugger::get_pc() {
  return get_register_value(m_pid, minidbg::reg::rip);
}

void debugger::set_pc(std::uint64_t pc) {
  set_register_value(m_pid, minidbg::reg::rip, pc);
}

void debugger::step_over_breakpoint() {
  auto pc = static_cast<std::intptr_t>(get_pc());
  if (m_breakpoints.count(pc)) {
    auto&& bp = m_breakpoints[pc];
    if (bp.is_enabled()) {
      bp.disable();
      ptrace(PTRACE_SINGLESTEP, m_pid, nullptr, nullptr);
      wait_for_signal();
      bp.enable();
    }
  }
}

void debugger::wait_for_signal() {
  int wait_status;
  auto options = 0;
  waitpid(m_pid, &wait_status, options);
  auto siginfo = get_signal_info();
  switch (siginfo.si_signo) {
    case SIGTRAP:
      handle_sigtrap(siginfo);
      break;
    case SIGSEGV:
      std::cout << "Yay, segfault. Reason: " << siginfo.si_code << '\n';
      break;
    default:
      std::cout << "Got signal " << strsignal(siginfo.si_signo) << '\n';
  }
}

dwarf::die debugger::get_function_from_pc(std::uint64_t pc) {
  for (auto& cu : m_dwarf.compilation_units()) {
    if (die_pc_range(cu.root()).contains(pc)) {
      for (const auto& die : cu.root()) {
        if (die.tag == dwarf::DW_TAG::subprogram) {
          if (die_pc_range(die).contains(pc)) {
            return die;
          }
        }
      }
    }
  }
  throw std::out_of_range("Cannot find function");
}

dwarf::line_table::iterator debugger::get_line_entry_from_pc(std::uint64_t pc) {
  for (auto& cu : m_dwarf.compilation_units()) {
    if (die_pc_range(cu.root()).contains(pc)) {
      auto& lt = cu.get_line_table();
      auto it = lt.find_address(pc);
      if (it == lt.end()) {
        throw std::out_of_range("Cannot find line entry in line table");
      } else {
        return it;
      }
    }
  }
  throw std::out_of_range("Cannot find line entry");
}

void debugger::initialize_load_address() {
  if (m_elf.get_hdr().type == elf::et::dyn) {
    std::ifstream map("/proc/" + std::to_string(m_pid) + "/maps");
    std::string addr;
    std::getline(map, addr, '-');

    m_load_address = std::stoul(addr, 0, 16);
  }
}

std::uint64_t debugger::offset_load_address(std::uint64_t addr) {
  return addr - m_load_address;
}

void debugger::step_over() {
  auto func = get_function_from_pc(get_offset_pc());
  auto func_entry = at_low_pc(func);
  auto func_end = at_high_pc(func);

  auto line = get_line_entry_from_pc(func_entry);
  auto start_line = get_line_entry_from_pc(get_offset_pc());

  std::vector<std::intptr_t> to_delete{};

  while (line->address < func_end) {
    auto load_address =
        static_cast<std::intptr_t>(offset_dwarf_address(line->address));
    if (line->address != start_line->address &&
        !m_breakpoints.count(load_address)) {
      set_breakpoint_at_address(load_address);
      to_delete.push_back(load_address);
    }
    ++line;
  }

  auto frame_pointer = get_register_value(m_pid, minidbg::reg::rbp);
  auto return_address =
      static_cast<std::intptr_t>(read_memory(frame_pointer + 8));

  if (!m_breakpoints.count(return_address)) {
    set_breakpoint_at_address(return_address);
    to_delete.push_back(return_address);
  }

  continue_execution();

  for (const auto addr : to_delete) {
    remove_breakpoint(addr);
  }
}

void debugger::print_source(const std::string& file_name, unsigned int line,
                            unsigned int n_lines_context) {
  std::ifstream file(file_name);
  auto start_line = line <= n_lines_context ? 1 : line - n_lines_context;
  auto end_line = line + n_lines_context +
                  (line < n_lines_context ? n_lines_context - line : 0) + 1;
  char c{};
  auto current_line = 1u;
  while (current_line != start_line && file.get(c)) {
    if (c == '\n') {
      ++current_line;
    }
  }
  std::cout << (current_line == line ? "> " : "  ");

  while (current_line <= end_line && file.get(c)) {
    std::cout << c;
    if (c == '\n') {
      ++current_line;
      std::cout << (current_line == line ? "> " : "  ");
    }
  }
  std::cout << '\n';
}

siginfo_t debugger::get_signal_info() {
  siginfo_t info;
  ptrace(PTRACE_GETSIGINFO, m_pid, nullptr, &info);
  return info;
}

void debugger::handle_sigtrap(siginfo_t info) {
  switch (info.si_code) {
    case SI_KERNEL:
    case TRAP_BRKPT: {
      set_pc(get_pc() - 1);
      std::cout << "Hit breakpoint at address 0x" << std::hex << get_pc()
                << '\n';
      auto offset_pc = offset_load_address(get_pc());
      auto line_entry = get_line_entry_from_pc(offset_pc);
      print_source(line_entry->file->path, line_entry->line);
      return;
    }
    case TRAP_TRACE:
      return;
    default:
      std::cout << "Unknown SIGTRAP code " << info.si_code << '\n';
      return;
  }
}

void debugger::single_step_instruction() {
  ptrace(PTRACE_SINGLESTEP, m_pid, nullptr, nullptr);
  wait_for_signal();
}

void debugger::single_step_instruction_with_breakpoint_check() {
  if (m_breakpoints.count(static_cast<std::intptr_t>(get_pc()))) {
    step_over_breakpoint();
  } else {
    single_step_instruction();
  }
}

void debugger::step_out() {
  auto frame_pointer = get_register_value(m_pid, minidbg::reg::rbp);
  auto return_address =
      static_cast<std::intptr_t>(read_memory(frame_pointer + 8));

  bool should_remove_breakpoint = false;
  if (!m_breakpoints.count(return_address)) {
    set_breakpoint_at_address(return_address);
    should_remove_breakpoint = true;
  }

  continue_execution();

  if (should_remove_breakpoint) {
    remove_breakpoint(return_address);
  }
}

void debugger::remove_breakpoint(const std::intptr_t addr) {
  if (m_breakpoints.at(addr).is_enabled()) {
    m_breakpoints.at(addr).disable();
  }
  m_breakpoints.erase(addr);
}

void debugger::step_in() {
  auto line = get_line_entry_from_pc(get_offset_pc())->line;

  while (get_line_entry_from_pc(get_offset_pc())->line == line) {
    single_step_instruction_with_breakpoint_check();
  }

  auto line_entry = get_line_entry_from_pc(get_offset_pc());
  print_source(line_entry->file->path, line_entry->line);
}

std::uint64_t debugger::get_offset_pc() {
  return offset_load_address(get_pc());
}

std::uint64_t debugger::offset_dwarf_address(std::uint64_t addr) {
  return addr + m_load_address;
}

std::vector<symbol> debugger::lookup_symbol(const std::string& name) {
  std::vector<symbol> syms;

  for (const auto& sec : m_elf.sections()) {
    if (sec.get_hdr().type != elf::sht::symtab &&
        sec.get_hdr().type != elf::sht::dynsym) {
      continue;
    }

    for (const auto sym : sec.as_symtab()) { /* NOLINT */
      if (sym.get_name() == name) {
        auto& d = sym.get_data();
        syms.push_back(symbol{.type = to_symbol_type(d.type()),
                              .name = sym.get_name(),
                              .addr = d.value});
      }
    }
  }
  return syms;
}

void debugger::print_backtrace() {
  auto output_frame = [frame_number = 0](auto&& func) mutable {
    std::cout << "frame #" << frame_number++ << ": 0x" << dwarf::at_low_pc(func)
              << ' ' << dwarf::at_name(func) << '\n';
  };

  auto current_func = get_function_from_pc(offset_load_address(get_pc()));
  output_frame(current_func);

  auto frame_pointer = get_register_value(m_pid, minidbg::reg::rbp);
  auto return_address = read_memory(frame_pointer + 8);

  while (dwarf::at_name(current_func) != "main") {
    current_func = get_function_from_pc(offset_load_address(return_address));
    output_frame(current_func);
    frame_pointer = read_memory(frame_pointer);
    return_address = read_memory(frame_pointer + 8);
  }
}

void debugger::read_variables() {
  auto func = get_function_from_pc(get_offset_pc());

  for (const auto& die : func) {
    if (die.tag == dwarf::DW_TAG::variable) {
      auto loc_val = die[dwarf::DW_AT::location];
      if (loc_val.get_type() == dwarf::value::type::exprloc) {
        ptrace_expr_content context(m_pid);
        auto result = loc_val.as_exprloc().evaluate(&context);
        switch (result.location_type) {
          case dwarf::expr_result::type::address: {
            auto offset_addr = result.value;
            auto value = read_memory(offset_addr);
            std::cout << at_name(die) << " (0x" << std::hex << offset_addr
                      << ") = " << value << std::endl;
            break;
          }
          case dwarf::expr_result::type::reg: {
            auto value = get_register_value_from_dwarf_register(
                m_pid, static_cast<unsigned int>(result.value));
            std::cout << at_name(die) << " (reg " << result.value
                      << ") = " << value << std::endl;
            break;
          }
          default:
            throw std::runtime_error("Unhandled variable location (1)");
        }
      } else {
        throw std::runtime_error("Unhandled variable location (2)");
      }
    }
  }
}

}  // namespace minidbg

void execute_debugee(const std::string& prog_name) {
  if (ptrace(PTRACE_TRACEME, 0, 0, 0) < 0) {
    std::cerr << "Error in ptrace\n";
    return;
  }
  execl(prog_name.c_str(), prog_name.c_str(), nullptr);
}

int main(int argc, char* argv[]) {
  if (argc < 2) {
    std::cerr << "Program name not specified";
    return -1;
  }

  auto prog = argv[1];

  auto pid = fork();
  if (pid == 0) {
    personality(ADDR_NO_RANDOMIZE);
    execute_debugee(prog);
  } else if (pid >= 1) {
    std::cout << "Started debugging process " << pid << '\n';
    minidbg::debugger dbg(prog, pid);
    dbg.run();
  }
}
